﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Fuzzy;

namespace ServerRoom
{
    static class Program
    {
        private const double Resolution = 0.1;

        static void Main()
        {
            var tempUniverse = new Universe(0, 50, Resolution);
            var deltaTempUniverse = new Universe(-25, 25, Resolution);
            var fanSpeedUnivers = new Universe(0, 100, Resolution);

            var tempLing = new LinguisticVariable("temp", tempUniverse);
            AddMemberships(tempLing, "very_cold", "cold", "mild", "hot", "very_hot");
            using (var f = new StreamWriter("temp.xls"))
            {
                f.Write("temp");
                foreach (var allFuzzyNumber in tempLing.AllFuzzyNumbers)
                    f.Write($"\t{allFuzzyNumber.Key}");
                f.WriteLine();
                for (int i = 0; i < tempLing["very_cold"].Memberships[0].Length; i++)
                {
                    f.Write($"{tempLing["very_cold"].Memberships[0][i]}");
                    foreach (var tempLingAllSet in tempLing.AllFuzzyNumbers)
                        f.Write($"\t{tempLingAllSet.Value.Memberships[1][i]}");
                    f.WriteLine();
                }
            }

            var deltaTempLing = new LinguisticVariable("delta_temp", deltaTempUniverse);
            AddMemberships(deltaTempLing, "negative_large", "negative_small", "approximately_zero",
                "positive_small", "positive_large");
            using (var f = new StreamWriter("delta_temp.xls"))
            {
                f.Write("delta_temp");
                foreach (var allFuzzyNumber in deltaTempLing.AllFuzzyNumbers)
                    f.Write($"\t{allFuzzyNumber.Key}");
                f.WriteLine();
                for (int i = 0; i < deltaTempLing["negative_large"].Memberships[0].Length; i++)
                {
                    f.Write($"{deltaTempLing["negative_large"].Memberships[0][i]}");
                    foreach (var tempLingAllSet in deltaTempLing.AllFuzzyNumbers)
                        f.Write($"\t{tempLingAllSet.Value.Memberships[1][i]}");
                    f.WriteLine();
                }
            }


            var fanSpeedLing = new LinguisticVariable("fan_speed", fanSpeedUnivers);
            AddMemberships(fanSpeedLing, "approximately_zero", "very_slow", "slow", "fast", "very_fast");
            using (var f = new StreamWriter("fan_speed.xls"))
            {
                f.Write("fan_speed");
                foreach (var allFuzzyNumber in fanSpeedLing.AllFuzzyNumbers)
                    f.Write($"\t{allFuzzyNumber.Key}");
                f.WriteLine();
                for (int i = 0; i < fanSpeedLing["approximately_zero"].Memberships[0].Length; i++)
                {
                    f.Write($"{fanSpeedLing["approximately_zero"].Memberships[0][i]}");
                    foreach (var tempLingAllSet in fanSpeedLing.AllFuzzyNumbers)
                        f.Write($"\t{tempLingAllSet.Value.Memberships[1][i]}");
                    f.WriteLine();
                }
            }


            var controller = new Controller();
            controller.AddAntecedent(tempLing);
            controller.AddAntecedent(deltaTempLing);
            controller.SetOutput(fanSpeedLing);

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"very_cold", "cold"}),
                new RulePart("delta_temp", new[] {"negative_large", "negative_small", "approximately_zero"})
            }, "approximately_zero"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"very_cold"}),
                new RulePart("delta_temp", new[] {"positive_small", "positive_large"})
            }, "approximately_zero"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"cold"}),
                new RulePart("delta_temp", new[] {"positive_small", "positive_large"})
            }, "approximately_zero"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"mild"}),
                new RulePart("delta_temp", new[] {"negative_small", "negative_large", "approximately_zero"})
            }, "very_slow"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"cold"}),
                new RulePart("delta_temp", new[] {"positive_small"})
            }, "slow"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"cold"}),
                new RulePart("delta_temp", new[] {"positive_large"})
            }, "fast"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"hot", "very_hot"}),
                new RulePart("delta_temp", new[] {"negative_small", "negative_large"})
            }, "fast"));

            controller.AddRule(new Rule(new[]
            {
                new RulePart("temp", new[] {"hot", "very_hot"}),
                new RulePart("delta_temp", new[] {"positive_small", "positive_large", "approximately_zero"})
            }, "very_fast"));


            TestController(controller, tempUniverse, deltaTempUniverse);
            //var inpTemp = new FuzzyNumber(tempUniverse, CreateFromAb(25, 10));
            //var inpDeltaTemp = new FuzzyNumber(deltaTempUniverse,
            //    CreateFromAb(-2, 4));
            //var result = controller.Compute(new Tuple<string, FuzzyNumber>("temp", inpTemp),
            //    new Tuple<string, FuzzyNumber>("delta_temp", inpDeltaTemp));

            //Console.WriteLine(result.Defuzzify());


            Console.ReadKey();
        }

        private static TriangularMembership CreateFromAb(double a, double b)
        {
            return new TriangularMembership(a - b / 2, a, a + b / 2);
        }

        private static void AddMemberships(LinguisticVariable variable, params string[] lings)
        {
            var t = (variable.Universe.Max - variable.Universe.Min) / (lings.Length - 1);
            variable.AddMembership(lings[0],
                new TriangularMembership(variable.Universe.Min, variable.Universe.Min, variable.Universe.Min + t));
            for (var i = 1; i < lings.Length - 1; i++)
                variable.AddMembership(lings[i],
                    new TriangularMembership(
                        variable.Universe.Min + (i - 1) * t,
                        variable.Universe.Min + i * t,
                        variable.Universe.Min + (i + 1) * t
                    )
                );

            variable.AddMembership(lings[lings.Length - 1],
                new TriangularMembership(variable.Universe.Max - t, variable.Universe.Max, variable.Universe.Max));
        }
        private static void TestController(Controller controller, Universe tempUniverse, Universe deltaTempUniverse)
        {
            var res = 0.5;
            var tempTol = 3;
            var deltaTempTol = 3;
            using (var file = new StreamWriter("result.xls"))
            {
                foreach (var d in Set.Range(-10, 10, res))
                {
                    file.Write("\t" + d);
                }
                file.WriteLine();

                foreach (var temp in Set.Range(0, 50, res))
                {
                    file.Write($"{temp}");
                    foreach (var deltaTemp in Set.Range(-10, 10, res))
                    {
                        var inpTemp = new FuzzyNumber(tempUniverse,
                            CreateFromAb(temp, tempTol));
                        var inpDeltaTemp = new FuzzyNumber(deltaTempUniverse,
                            CreateFromAb(deltaTemp, deltaTempTol));
                        var result = controller.Compute(new Tuple<string, FuzzyNumber>("temp", inpTemp),
                            new Tuple<string, FuzzyNumber>("delta_temp", inpDeltaTemp));

                        var txt =
                            $"{temp}\t{deltaTemp}\t{result.Defuzzify()}";

                        Console.WriteLine(txt);
                        file.Write($"\t{result.Defuzzify()}");
                    }
                    file.WriteLine();
                }
            }
        }
    }
}
