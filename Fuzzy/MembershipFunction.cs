﻿namespace Fuzzy
{
    public abstract class MembershipFunction : IMembership
    {
        public abstract double GetMembership(double value);
    }
}