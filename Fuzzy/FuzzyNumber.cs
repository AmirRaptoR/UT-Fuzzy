﻿using System;
using System.Linq;

namespace Fuzzy
{
    public class FuzzyNumber
    {
        private readonly Intersection _intersection;
        private readonly double[][] _memberships;
        private readonly Union _union;

        public double this[double index]
        {
            get
            {
                int ind = (int)((index - Universe.Min) / Universe.Resolution);
                return _memberships[1][ind];
            }
        }

        public double[][] Memberships => _memberships;

        public FuzzyNumber(Universe universe, IMembership membership,
            Intersection intersection = null, Union union = null)
        {
            _intersection = intersection ?? Intersections.NormalIntersection;
            _union = union ?? Unios.NormalUnio;
            Universe = universe;
            _memberships = new double[2][];
            _memberships[0] = new double[universe.Points];
            _memberships[1] = new double[universe.Points];
            var index = 0;
            foreach (var d in Set.Range(universe.Min, universe.Max, universe.Resolution))
            {
                _memberships[0][index] = d;
                _memberships[1][index] = membership.GetMembership(d);
                index++;
            }
        }

        internal FuzzyNumber(Universe universe, double[][] memberships, Intersection intersection = null,
            Union union = null)
        {
            Universe = universe;
            _memberships = memberships;
            _intersection = intersection ?? Intersections.NormalIntersection;
            _union = union ?? Unios.NormalUnio;
        }

        public Universe Universe { get; }

        public FuzzyNumber Intersect(FuzzyNumber number)
        {
            if (number.Universe != Universe)
                throw new Exception("Can not intersect with different universes");
            var mems = new double[2][];
            mems[0] = new double[Universe.Points];
            mems[1] = new double[Universe.Points];
            var index = 0;
            foreach (var d in Set.Range(Universe.Min, Universe.Max, Universe.Resolution))
            {
                mems[0][index] = d;
                mems[1][index] = _intersection(_memberships[1][index], number._memberships[1][index]);
                index++;
            }
            return new FuzzyNumber(Universe, mems);
        }

        public FuzzyNumber Union(FuzzyNumber number)
        {
            if (number.Universe != Universe)
                throw new Exception("Can not intersect with different universes");
            var mems = new double[2][];
            mems[0] = new double[Universe.Points];
            mems[1] = new double[Universe.Points];
            var index = 0;
            foreach (var d in Set.Range(Universe.Min, Universe.Max, Universe.Resolution))
            {
                mems[0][index] = d;
                mems[1][index] = _union(_memberships[1][index], number._memberships[1][index]);
                index++;
            }
            return new FuzzyNumber(Universe, mems);
        }

        public FuzzyNumber Cut(double value)
        {
            var mems = new double[2][];
            mems[0] = new double[Universe.Points];
            mems[1] = new double[Universe.Points];
            var index = 0;
            foreach (var d in Set.Range(Universe.Min, Universe.Max, Universe.Resolution))
            {
                mems[0][index] = d;
                mems[1][index] = Math.Min(_memberships[1][index], value);
                index++;
            }
            return new FuzzyNumber(Universe, mems);
        }

        public double Defuzzify(Defuzzifier defuzzifier = null)
        {
            defuzzifier = defuzzifier ?? Defuzzifiers.MeanOfMaxima;
            return defuzzifier(this);
        }

        public double Height()
        {
            return _memberships[1].Max();
        }
    }

}