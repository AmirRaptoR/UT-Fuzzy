﻿using System.Collections.Generic;

namespace Fuzzy
{
    public static class Set
    {
        public static IEnumerable<double> Range(double start, double end, double steps)
        {
            var s = start;
            int index = 0;
            while (s < end)
            {
                s = start + (index++) * steps;
                yield return s;
            }
        }

        public static IEnumerable<double> Range(double end)
        {
            return Range(0, end, 1);
        }
        public static IEnumerable<double> Range(double start, double end)
        {
            return Range(start, end, 1);
        }

    }
}