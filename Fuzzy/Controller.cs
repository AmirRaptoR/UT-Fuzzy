﻿using System;
using System.Collections.Generic;
using System.Linq;
using Charts;

namespace Fuzzy
{
    public class RulePart
    {
        public RulePart(string name, IEnumerable<string> variables)
        {
            Name = name;
            Variables = variables;
        }

        public string Name { get; }
        public IEnumerable<string> Variables { get; }
    }

    public class Rule
    {
        public Rule(IEnumerable<RulePart> conditions, string output)
        {
            Conditions = conditions;
            Output = output;
        }

        public IEnumerable<RulePart> Conditions { get; }
        public string Output { get; set; }
    }

    public class Controller
    {
        private readonly Dictionary<string, LinguisticVariable> _inputs = new Dictionary<string, LinguisticVariable>();
        private Tuple<string, LinguisticVariable> _output;
        private readonly List<Rule> _rules = new List<Rule>();

        public void AddRule(Rule rule)
        {
            _rules.Add(rule);
        }

        public void AddAntecedent(LinguisticVariable lingVariable)
        {
            _inputs.Add(lingVariable.Name, lingVariable);
        }

        public void SetOutput(LinguisticVariable lingVariable)
        {
            _output = new Tuple<string, LinguisticVariable>(lingVariable.Name, lingVariable);
        }

        public FuzzyNumber Compute(params Tuple<string, FuzzyNumber>[] inputs)
        {
            var inps = inputs.ToDictionary(x => x.Item1, x => x.Item2);

            var lst = _rules.Select((rule, ruleIndex) =>
            {
                //                var tmpLst = inputs.Select(x => x.Item2.Memberships).ToList();
                //                var titleNames = inputs.Select(x => x.Item1).ToList();
                var tmpLst = new List<double[][]>();
                var titleNames = new List<string>();

                var alpha = rule.Conditions.Min(x =>
                  {
                      var united = x.Variables.Select(v => _inputs[x.Name][v]).Aggregate((a, b) => a.Union(b));

                      tmpLst.Add(united.Memberships);
                      titleNames.Add("-");

                      var height = united.Intersect(inps[x.Name]).Height();
                      return height;
                  });
                var cutted = _output.Item2[rule.Output].Cut(alpha);

                tmpLst.Add(cutted.Memberships);
                titleNames.Add($"o {rule.Output}");

                tmpLst.Add(cutted.Memberships);
                titleNames.Add($"r{ruleIndex} -o {rule.Output}");
                //                var t = rule.Conditions.SelectMany(x => x.Variables.Select(s => _inputs[x.Name][s].Memberships));
                //                tmpLst.AddRange(t);
                ChartHelper.AddPlot($"{string.Join(",", rule.Conditions.Select(x => x.Name))} - union", tmpLst, titleNames);
                return cutted;
            }).ToArray();

            var tmp = lst.Aggregate((a, b) => a.Union(b));

            return tmp;
        }
    }
}