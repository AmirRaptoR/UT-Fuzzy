﻿using System;
using System.Linq;

namespace Fuzzy
{
    public static class Intersections
    {
        public static double NormalIntersection(double v1, double v2)
        {
            return Math.Min(v1, v2);
        }
    }

    public static class Unios
    {
        public static double NormalUnio(double v1, double v2)
        {
            return Math.Max(v1, v2);
        }
    }


    public static class Defuzzifiers
    {
        public static double MeanOfMaxima(FuzzyNumber number)
        {
            var max = number.Memberships[1].Max();
            var s = 0.0;
            var count = 0;
            for (int i = 0; i < number.Memberships[0].Length; i++)
            {
                if (Math.Abs(number.Memberships[1][i] - max) < 0.000001)
                {
                    s += number.Memberships[0][i];
                    count++;
                }
            }
            return s / count;
        }
    }
}