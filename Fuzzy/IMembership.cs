﻿namespace Fuzzy
{
    public interface IMembership
    {
        double GetMembership(double value);
    }
}