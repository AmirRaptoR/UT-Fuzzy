﻿namespace Fuzzy
{
    public class Universe
    {
        public Universe(double min, double max, double resolution)
        {
            Resolution = resolution;
            Min = min;
            Max = max;
        }

        public double Max { get; }
        public double Min { get; }
        public double Resolution { get; }


        public int Points => (int)((Max - Min) / Resolution + 0.5) + 1;
    }
}