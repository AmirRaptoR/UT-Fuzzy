﻿using System.Collections.Generic;

namespace Fuzzy
{
    public class LinguisticVariable
    {
        private readonly Dictionary<string, FuzzyNumber> _numbers;

        public Dictionary<string, FuzzyNumber> AllFuzzyNumbers => _numbers;

        public IEnumerable<FuzzyNumber> AllSets => _numbers.Values;

        public FuzzyNumber this[string index] => _numbers[index];

        public LinguisticVariable(string name, Universe universe)
        {
            _numbers = new Dictionary<string, FuzzyNumber>();
            Name = name;
            Universe = universe;
        }

        public string Name { get; }
        public Universe Universe { get; }

        public void AddMembership(string name, IMembership membership)
        {
            _numbers.Add(name, new FuzzyNumber(Universe, membership));
        }


    }
}