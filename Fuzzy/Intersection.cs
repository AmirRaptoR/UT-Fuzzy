﻿namespace Fuzzy
{
    public delegate double Intersection(double value1, double value2);
    public delegate double Union(double value1, double value2);
}