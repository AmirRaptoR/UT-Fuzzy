﻿using System;

namespace Fuzzy
{
    public class TriangularMembership : MembershipFunction
    {
        private readonly double _a;
        private readonly double _b;
        private readonly double _c;

        public TriangularMembership(double a, double b, double c)
        {
            _a = a;
            _b = b;
            _c = c;
            if (a > b)
                throw new Exception("a must be smaller than b");
            if (b > c)
                throw new Exception("b must be smaller than c");
        }


        public override double GetMembership(double value)
        {
            if (_a == _c && value==_a)
            {
                return 1.0;
            }
            if (value < _a || value > _c)
                return 0;

            if (value < _b)
                return (value - _a) / (_b - _a);
            return (_c - value) / (_c - _b);
        }


    }
}