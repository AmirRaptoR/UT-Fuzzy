﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace Charts
{
    public static class ChartHelper
    {
        public static List<PlotView> Plots = new List<PlotView>();

        public static void AddPlot(string name, double[][] data)
        {
            return;
            var plot = new PlotView
            {
                Name = name,
                Model = new PlotModel
                {
                    Title = name
                }
            };
            var lineSerie = new AreaSeries()
            {
                Font = "Arial",
            };
            lineSerie.Points.AddRange(data[0].Select((x, i) => new DataPoint(data[0][i], data[1][i])));
            plot.Model.Series.Add(lineSerie);

            Plots.Add(plot);
        }

        public static void AddPlot(string name, IEnumerable<double[][]> datas, IList<string> titles = null)
        {
            return;
            var plot = new PlotView
            {
                Name = name,
                Model = new PlotModel()
            };

            int index = 0;
            foreach (var data in datas)
            {
                var lineSerie = new LineSeries
                {
                    Font = "Arial",
                    Title = (titles != null && index < titles.Count) ? titles[index] : "Plot " + index
                };
                lineSerie.Points.AddRange(data[0].Select((x, i) => new DataPoint(data[0][i], data[1][i])));
                plot.Model.Series.Add(lineSerie);
                index++;
            }

            Plots.Add(plot);
        }


    }
}
