﻿using System.Windows.Forms;
using OxyPlot;
using OxyPlot.WindowsForms;

namespace Charts
{
    public partial class PlotForm : Form
    {
        public PlotForm(PlotView plot)
        {
            InitializeComponent();

            plot.Dock = DockStyle.Fill;

            Controls.Add(plot);
        }

        public static PlotForm Show(PlotModel model)
        {
            var plot = new PlotView {Model = model};
            return new PlotForm(plot);
        }
    }
}